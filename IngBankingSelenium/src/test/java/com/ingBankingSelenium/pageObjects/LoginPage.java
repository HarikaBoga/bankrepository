package com.ingBankingSelenium.pageObjects;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.CacheLookup;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class LoginPage {

	WebDriver ldriver;

	public LoginPage(WebDriver rdriver) {
		ldriver = rdriver;
		PageFactory.initElements(rdriver, this);

	}

	@FindBy(name = "uid")
	@CacheLookup
	WebElement textUserName;

	@FindBy(name = "password")
	@CacheLookup
	WebElement textPassword;

	@FindBy(name = "login")
	@CacheLookup
	WebElement btnLogin;

	@FindBy(xpath = "btnReset")
	@CacheLookup
	WebElement btnReset;

	public void setUserName(String uname) {
		textUserName.sendKeys(uname);
	}

	public void setPassword(String pwd) {
		textPassword.sendKeys(pwd);
	}

	public void clickSubmit() {
		btnLogin.click();

	}

	public void clickReset() {

		btnReset.click();
	}
}
