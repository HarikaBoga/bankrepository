package com.ingBankingSelenium.pageObjects;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.CacheLookup;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class FacebookLoginPage {
	WebDriver ldriver;

	public FacebookLoginPage(WebDriver rdriver) {
		ldriver = rdriver;
		PageFactory.initElements(rdriver, this);
	}

	@FindBy(name = "email")
	@CacheLookup
	WebElement textUserName;

	@FindBy(name = "pass")
	@CacheLookup
	WebElement textPassword;

	@FindBy(name = "login")
	@CacheLookup
	WebElement btnLogin;

	public void setUserName(String uname) {
		textUserName.sendKeys(uname);
	}

	public void setPassword(String pwd) {
		textPassword.sendKeys(pwd);
	}

	public void clickSubmit() {
		btnLogin.click(); 

	}

}
