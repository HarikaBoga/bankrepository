package com.ingBankingSelenium.TestCases;

import java.io.IOException;

import org.testng.Assert;
import org.testng.annotations.Test;

import com.ingBankingSelenium.pageObjects.FacebookLoginPage;

public class TC_LoginTest_001 extends LoginBaseClass {
	@Test
	public void loginTest() throws IOException {

		logger.info("URL is Opened");
		FacebookLoginPage lp = new FacebookLoginPage(driver);
		lp.setUserName(username);
		logger.info("Enter username");
		lp.setPassword(password);
		logger.info("Enter pasword");
		lp.clickSubmit();
		logger.info("click pn login page");
		if (driver.getTitle().equals("kfjgnjkfxdih")) {
			Assert.assertTrue(true);
			logger.info("login test passed");
		} else {
			captureScreenShot(driver,"logintest");
			Assert.assertFalse(false);
			logger.info("login test failed");
		}
	}

}
