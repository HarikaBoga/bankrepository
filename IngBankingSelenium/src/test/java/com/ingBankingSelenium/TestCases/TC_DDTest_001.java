package com.ingBankingSelenium.TestCases;

import java.io.IOException;
import org.openqa.selenium.NoAlertPresentException;
import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import com.ingBankingSelenium.Utilities.XLUtilies;
import com.ingBankingSelenium.pageObjects.LoginPage;


public class TC_DDTest_001 extends LoginBaseClass {
	@Test(dataProvider = "LoginData")
	public void loginDDT(String user, String pwd) throws IOException, InterruptedException {
		LoginPage lp = new LoginPage(driver);
		logger.info("user name provided");
		lp.setUserName(user);
		logger.info("password is provided");
		lp.setPassword(pwd);
		logger.info("submitting Details");
		Thread.sleep(5000);
		lp.clickSubmit();
		if (isAlertPresent() == true) {
			captureScreenShot(driver, "datadrivenlogintest");
			driver.switchTo().alert().accept();
			driver.switchTo().defaultContent();
			Assert.assertTrue(false);
			logger.warn("Invalid Credentials");
		} else {
			captureScreenShot(driver, "datadrivenlogintest");
			driver.switchTo().alert().accept();
			lp.clickReset();
			Assert.assertTrue(true);
			logger.warn("valid Credentials");

		}

	}

	public boolean isAlertPresent()// userdefined Method
	{
		try {
			driver.switchTo().alert();
			return true;

		} catch (NoAlertPresentException e) {
			return false;

		}

	}

	@DataProvider(name = "LoginData")
	String[][] getData() throws IOException {
		String path = System.getProperty("user.dir") + "/src/test/java/com/ingBankingSelenium/testData/LoginData.xlsx";
		int rownum = XLUtilies.getRowCount(path, "sheet1");
		int colcount = XLUtilies.getCellCount(path, "sheet1", 1);
		String logindata[][] = new String[rownum][colcount];
		for (int i = 1; i <= rownum; i++) {
			for (int j = 0; j < colcount; j++) {
				logindata[i - 1][j] = XLUtilies.getCellData(path, "sheet1", i, j);

			}
		}

		return logindata;

	}

}
