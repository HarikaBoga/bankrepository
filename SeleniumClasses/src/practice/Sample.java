package practice;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

public class Sample {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		System.setProperty("webdriver.chrome.driver",
				"C:\\Users\\Akhila Edla\\Downloads\\chromedriver_win32\\chromedriver.exe");
		WebDriver driver = new ChromeDriver();
		driver.get("https://www.facebook.com/");
		driver.manage().deleteAllCookies();
		driver.manage().window().maximize();
		String title = driver.getTitle();
		System.out.println(title);
		System.out.println(driver.getCurrentUrl());
		// driver.findElement(By.id("email"));
		/*
		 * driver.findElement(By.id("email")).sendKeys("9441020461");
		 * driver.findElement(By.name("pass")).sendKeys("Haari@21225");
		 * 
		 */

		// driver.findElement(By.className("inputtext _55r1
		// _6luy")).sendKeys("9441020461");
		// this class name is same for both email and password thats why we are not
		// getting proper responsnse

		// driver.findElement(By.tagName("input")).sendKeys("Haari@21225");
		// It won't work .Because same tagname should be prasent for morethan input
		// fields.
		/*
		 * driver.findElement(By.linkText("Forgotten password?")).click();
		 * driver.findElement(By.id("identify_email")).sendKeys("9441020461");
		 * driver.findElement(By.id("did_submit")).click();
		 */
		// driver.findElement(By.partialLinkText("Forgotten")).click();
		driver.findElement(By.cssSelector("#pass")).sendKeys("Haari@21225");
		driver.findElement(By.xpath("//*[@id=\"email\"]")).sendKeys("9441020461");
		driver.findElement(By.xpath("//*[@id=\"reg_pages_msg\"]/a")).click();
	}

}
